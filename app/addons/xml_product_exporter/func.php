<?php


use Tygh\Registry;

/**
 * Generate URL for XML product exporter.
 *
 * @param string $provider The provider name.
 * @return string The generated URL.
 */
function fn_get_url_xml_product_exporter(string $provider): string
{
    $providerConfig = fn_get_config_provider_xml_product_exporter();
    $base_lang_code = $providerConfig[$provider]['base_lang_code'];
    return fn_url('index.php?dispatch=xml_product_exporter&mode=xml&provider=' . $provider . '&0=&sl=' . $base_lang_code);
}

/**
 * Generate a message for XML product exporter.
 *
 * @param string $title The title of the message.
 * @param string $message The message content.
 * @return string The generated message HTML.
 */
function fn_get_msg_xml_product_exporter($title, $message)
{
    return '<div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px; margin-bottom: 10px;">
    <h4 style="margin-bottom: 10px;">' . $title . '</h4>
    <hr style="margin-top: 0; margin-bottom: 10px;">
        <div class="cart-body">
            ' .  $message . '
        </div>
    </div>';
}

/**
 * Generate a message with a link for exporting products to Salidzini.lv.
 *
 * @return string The generated message HTML.
 */
function fn_get_url_salidzini_lv()
{
    $link = fn_get_url_xml_product_exporter('salidzini_lv');
    return fn_get_msg_xml_product_exporter(sprintf(__('info_title'), fn_get_config_provider_xml_product_exporter()['salidzini_lv']['name']), sprintf(__('info_description'), $link));
}

/**
 * Generate a message with a link for exporting products to Kurpirkt.lv.
 *
 * @return string The generated message HTML.
 */
function fn_get_url_kurpirkt_lv()
{
    $link = fn_get_url_xml_product_exporter('kurpirkt_lv');
    return fn_get_msg_xml_product_exporter(sprintf(__('info_title'), fn_get_config_provider_xml_product_exporter()['kurpirkt_lv']['name']), sprintf(__('info_description'), $link));
}

/**
 * Get configuration parameters for XML product exporters.
 *
 * @return array Configuration parameters for XML product exporters.
 */
function fn_get_config_provider_xml_product_exporter(): array
{
    $brandIdFeature = Registry::get('addons.xml_product_exporter.brand_id_feature') ?? null;
    $modelIdFeature = Registry::get('addons.xml_product_exporter.model_id_feature') ?? null;
    $colorIdFeature = Registry::get('addons.xml_product_exporter.color_id_feature') ?? null;
    return [
        'salidzini_lv' => [
            'name' => 'Salidzini.lv',
            'class' => 'SalidziniLv',
            'base_lang_code' => Registry::get('addons.xml_product_exporter.salidzini_lv_base_lang_code') ?? 'en',
            'brand_id_feature' => $brandIdFeature,
            'model_id_feature' => $modelIdFeature,
            'color_id_feature' => $colorIdFeature,
            'category_id' => Registry::get('addons.xml_product_exporter.salidzini_lv_category_id') ?? 0,
            'export' => Registry::get('addons.xml_product_exporter.salidzini_lv_export') == 'Y' ? true : false,
            'export_subcategories' => Registry::get('addons.xml_product_exporter.salidzini_lv_export_subcategories') == 'Y' ? true : false,
            'limit' => Registry::get('addons.xml_product_exporter.salidzini_lv_limit') ?? 0,
            'price' => [
                'from' => Registry::get('addons.xml_product_exporter.salidzini_lv_priceFrom') ?? 1,
                'to' => Registry::get('addons.xml_product_exporter.salidzini_lv_priceTo') ?? 0,
            ],
            'quantity' => [
                'from' => Registry::get('addons.xml_product_exporter.salidzini_lv_quantityFrom') ?? 0,
                'to' => Registry::get('addons.xml_product_exporter.salidzini_lv_quantityTo') ?? 0,
            ],
            'hidden_products' => Registry::get('addons.xml_product_exporter.salidzini_lv_hiddenProducts') == 'Y' ? true : false,
        ],
        'kurpirkt_lv' => [
            'name' => 'Kurpirkt.lv',
            'class' => 'KurpirktLv',
            'base_lang_code' => Registry::get('addons.xml_product_exporter.salidzini_lv.kurpirkt_lv_base_lang_code') ?? 'en',
            'brand_id_feature' => $brandIdFeature,
            'model_id_feature' => $modelIdFeature,
            'color_id_feature' => $colorIdFeature,
            'category_id' => Registry::get('addons.xml_product_exporter.kurpirkt_lv_category_id') ?? 0,
            'export' => Registry::get('addons.xml_product_exporter.kurpirkt_lv_export') == 'Y' ? true : false,
            'export_subcategories' => Registry::get('addons.xml_product_exporter.kurpirkt_lv_export_subcategories') == 'Y' ? true : false,
            'limit' => Registry::get('addons.xml_product_exporter.kurpirkt_lv_limit') ?? 0,
            'price' => [
                'from' => Registry::get('addons.xml_product_exporter.kurpirkt_lv_priceFrom') ?? 1,
                'to' => Registry::get('addons.xml_product_exporter.kurpirkt_lv_priceTo') ?? 0,
            ],
            'quantity' => [
                'from' => Registry::get('addons.xml_product_exporter.kurpirkt_lv_quantityFrom') ?? 0,
                'to' => Registry::get('addons.xml_product_exporter.kurpirkt_lv_quantityTo') ?? 0,
            ],
            'hidden_products' => Registry::get('addons.xml_product_exporter.kurpirkt_lv_hiddenProducts') == 'Y' ? true : false,
        ],
    ];
}

function fn_get_all_categories()
{


    $category_tree = [
        0 => __('all_categories'),
    ];
    $categories = db_get_array("SELECT id_path FROM ?:categories WHERE status = ?s ORDER BY id_path ASC", 'A');

    foreach ($categories as $category) {
        $categoryExplode = explode('/', $category['id_path']);
        $categoryLastElement = end($categoryExplode);
        $category_tree[$categoryLastElement] = '';
        foreach ($categoryExplode as $categoryElement) {
            $category_name = db_get_field("SELECT category FROM ?:category_descriptions WHERE category_id = ?i AND lang_code = ?s", $categoryElement, CART_LANGUAGE);
            $category_tree[$categoryLastElement] .= $category_name . '/';
        }
    }

    return $category_tree;
}

function fn_settings_variants_addons_xml_product_exporter_salidzini_lv_category_id()
{
    return fn_get_all_categories();
}

function fn_settings_variants_addons_xml_product_exporter_kurpirkt_lv_category_id()
{
    return fn_get_all_categories();
}

function get_all_active_languages()
{
    $languages = [];
    $getLanguages = db_get_array('SELECT lang_code, name FROM ?:languages WHERE status = ?s', 'A');

    foreach ($getLanguages as $lang) {
        $languages[$lang['lang_code']] = $lang['name'];
    }

    return $languages;
}

function fn_settings_variants_addons_xml_product_exporter_salidzini_lv_base_lang_code()
{
    return get_all_active_languages();
}

function fn_settings_variants_addons_xml_product_exporter_kurpirkt_lv_base_lang_code()
{
    return get_all_active_languages();
}

function fn_get_all_features(string $firstElement = null)
{
    $features = [];
    if ($firstElement) {
        $features[0] = $firstElement;
    }
    $getProductFeaturesDescriptions = db_get_array('SELECT feature_id, description FROM ?:product_features_descriptions WHERE lang_code = ?s', CART_LANGUAGE);
    foreach ($getProductFeaturesDescriptions as $feature) {
        $features[$feature['feature_id']] = $feature['description'];
    }
    return $features;
}

function fn_settings_variants_addons_xml_product_exporter_brand_id_feature()
{
    return fn_get_all_features(__('no_feature_brand'));
}


function fn_settings_variants_addons_xml_product_exporter_model_id_feature()
{
    return fn_get_all_features(__('no_feature_model'));
}


function fn_settings_variants_addons_xml_product_exporter_color_id_feature()
{
    return fn_get_all_features(__('no_feature_color'));
}
