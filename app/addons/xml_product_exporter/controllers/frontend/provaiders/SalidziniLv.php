<?php


/**
 * This file is part of the XML Product Exporter addon.
 *
 * @package     Addons
 * @subpackage  XML Product Exporter
 */

// Prevent direct access to the file
defined('BOOTSTRAP') or die('Access denied');


/**
 * Class SalidziniLv
 *
 * This class handles the export of products to the Salidzini.lv platform in XML format.
 */
class SalidziniLv
{
    /**
     * @var SimpleXMLElement $xml The XML document to which products will be added.
     */
    protected $xml;

    /**
     * SalidziniLv constructor.
     *
     * @param SimpleXMLElement $xml The XML document to which products will be added.
     */
    public function __construct(SimpleXMLElement $xml)
    {
        $this->xml = $xml;
    }

    /**
     * Export products to Salidzini.lv XML format.
     *
     * @param array $products An array of products to export.
     *
     * @return SimpleXMLElement The XML document with added product nodes.
     */
    public function export($products)
    {
        // Add each product to the XML
        foreach ($products as $product) {

            $productNode = $this->xml->addChild('item');

            $productNode->addChild('name', htmlspecialchars($product['name'], ENT_QUOTES, 'UTF-8'));
            $productNode->addChild('url', $product['url']);
            $productNode->addChild('price', $product['price']);
            $productNode->addChild('category', htmlspecialchars(implode(' &gt;&gt; ', $product['categories'])));
            $productNode->addChild('category_url', $product['category_url']);

            if ($product['image'] != null) {
                $productNode->addChild('image', $product['image']);
            }

            $productNode->addChild('amount', $product['amount']);

            if ($product['brand'] != null) {
                $productNode->addChild('brand', $product['brand']);
            }

            if ($product['model'] != null) {
                $productNode->addChild('model', $product['model']);
            }

            if ($product['color'] != null) {
                $productNode->addChild('color', $product['color']);
            }

            $productNode->addChild('mpn', $product['mpn']);
            $productNode->addChild('gtin', $product['gtin']);
        }

        // Return the XML object
        return $this->xml;
    }
}
