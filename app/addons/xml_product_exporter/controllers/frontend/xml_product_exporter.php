<?php

/**
 * Prevent direct access to the file
 */
defined('BOOTSTRAP') or die('Access denied');

use Tygh\Addons\XmlProductExporter\ProductExporter;

require_once('provaiders/SalidziniLv.php');
require_once('provaiders/KurpirktLv.php');
require_once('ProductExporter.php');


/**
 * Array of supported providers with their configuration
 * 
 * @var array
 */
$providers = fn_get_config_provider_xml_product_exporter();

// Get mode and provider from the request
$mode = $_REQUEST['mode'] ?? null;
$provider = $_REQUEST['provider'] ?? null;

// Check if mode and provider are provided and not empty
if (empty($mode) || empty($provider)) {
    // Handle missing parameters
    throw new Exception(__('missing_parameters'));
}



// Check if mode is XML
if ($mode !== 'xml') {
    // Handle invalid mode
    throw new Exception(__('invalid_mode'));
}

// Check if the provider is supported
if (!isset($providers[$provider])) {
    // Handle invalid provider
    throw new Exception(__('invalid_provider'));
}

$providerParams = $providers[$provider];
$providerName = $providerParams["name"];
$price_from = $providerParams['price']['from'];
$price_to  = $providerParams['price']['to'];
$quantity_from = $providerParams['quantity']['from'];
$quantity_to  = $providerParams['quantity']['to'];

if ($price_from <= 0) {
    // Handle invalid price from zero or less
    throw new Exception(__('invalid_price_from_zero'));
}
if ($price_to != 0 && $price_from > $price_to) {
    // Handle invalid price range 
    throw new Exception(__('invalid_price_range'));
}

if ($quantity_from > $quantity_to) {
    // Handle invalid quantity range 
    throw new Exception(__('invalid_quantity_range'));
}

if ($providerParams['export'] === true) {
    // Get the provider class
    $providerNameClass = $providerParams['class'];

    // Check if the provider class exists
    if (!class_exists($providerNameClass)) {
        // Handle missing provider class
        throw new Exception(sprintf(__('invalid_provider_class'), $providerNameClass, $providerName));
    }

    $productsExports = new ProductExporter();
    $products = $productsExports->getProducts($providerParams, $providerParams['base_lang_code']);

    if (empty($products)) {
        // Handle empty products
        throw new Exception(__('empty_products'));
    }

    // Create an instance of the provider class
    $xmlString = '<?xml version="1.0" encoding="utf-8"?><root></root>';
    $xml = new SimpleXMLElement($xmlString);
    $provider = new $providerNameClass($xml);

    // Check if the export method exists in the provider class
    if (!method_exists($provider, 'export')) {
        // Handle missing export method
        throw new Exception(sprintf(__('invalid_export_method'), 'export', $providerName));
    }

    // Export products
    $xml = $provider->export($products);

    // Set the Content-Type header to specify that XML content will be returned
    header('Content-Type: application/xml; charset=utf-8');
    $xmlString = $xml->asXML();
    echo $xmlString;
    exit;
} else {
    // Handle export not enabled
    throw new Exception(sprintf(__('export_not_enabled'), $providerParams['name']));
}
