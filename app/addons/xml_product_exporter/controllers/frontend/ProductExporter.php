<?php

namespace Tygh\Addons\XmlProductExporter;

// Prevent direct access to the file
defined('BOOTSTRAP') or die('Access denied');

/**
 * Class ProductExporter
 * @package Tygh\Addons\XmlProductExporter
 */
class ProductExporter
{
    /**
     * Retrieves products based on provided parameters.
     *
     * @param array $providerParams Parameters for filtering products.
     * @param string $defaultLang Default language for URLs.
     * @return array An array containing product data.
     */
    public static function getProducts(array $providerParams, $defaultLang = 'en'): array
    {
        $products = array(); // Array to store products
        $productsResult = array();
        $params = [
            'custom_extend' => ['product_name', 'prices', 'categories', 'images'],
            'only_short_fields' => true,
            'area' => 'C',
            'get_frontend_urls' => true,
        ];

        // Set export limit if provided
        if (isset($providerParams['limit']) && $providerParams['limit'] > 0) {
            $params['limit'] = $providerParams['limit'];
        }

        // Set export subcategories if provided
        if (isset($providerParams['export_subcategories']) && $providerParams['export_subcategories'] == true) {
            $params['subcats'] = 'Y';
        }

        // Set category id if provided
        if (isset($providerParams['category_id']) && $providerParams['category_id'] > 0) {
            $params['cid'] = $providerParams['category_id'];
        }

        // Set price range if provided
        if (isset($providerParams['price']['from'])) {
            $params['price_from'] = max(0, $providerParams['price']['from']);

            if (isset($providerParams['price']['to']) && $providerParams['price']['to'] > 0) {
                $params['price_to'] = $providerParams['price']['to'];
            }
        }

        // Set quantity range if provided
        if (isset($providerParams['quantity']['from']) && $providerParams['quantity']['from'] > 0) {
            $params['amount_from'] = $providerParams['quantity']['from'];

            if (isset($providerParams['quantity']['to']) && $providerParams['quantity']['to'] > 0 && $providerParams['quantity']['from'] > 0) {
                $params['amount_to'] = $providerParams['quantity']['to'];
            }
        }

        // Include hidden products if specified
        if (isset($providerParams['hidden_products']) && $providerParams['hidden_products'] === true) {
            $params['status'] = 'A';
        }

        // Retrieve products based on parameters
        list($products) = fn_get_products($params, 0);
        fn_gather_additional_products_data($products, [
            'get_icon' => true,
            'get_detailed' => true,
            'get_additional' => true,
            'get_features' => true,
            'get_options' => true,
            'get_extra' => true,
        ]);

        // Iterate through products and format data
        foreach ($products as $product) {
            $product_id = $product["product_id"];
            $seo_path = $product["seo_path"];
            $brand_id_feature = $providerParams['brand_id_feature'];
            $model_id_feature = $providerParams['model_id_feature'];
            $color_id_feature = $providerParams['color_id_feature'];
            // echo '<pre>';
            // var_dump(self::get_features($product, $providerParams['brand_id_feature']));
            // // var_dump($product['product_features']);
            // exit();
            $productsResult[] = [
                'name' => $product['product'],
                'url' => fn_url("products.view?product_id=$product_id", AREA, 'current', $defaultLang),
                'price' =>  number_format($product['price'], 2, '.', ''),
                'categories' => self::category_full($seo_path, $defaultLang),
                'category_url' =>  fn_url("categories.view?category_id={$product['category_ids'][0]}", AREA, 'current', $defaultLang),
                'image' => self::get_image($product),
                'amount' => $product['amount'],
                'brand' => $brand_id_feature ? self::get_features($product, $brand_id_feature) : null,
                'model' => $model_id_feature ? self::get_features($product, $model_id_feature) : null,
                'color' => $color_id_feature ? self::get_features($product, $color_id_feature) : null,
                'mpn' => $product['product_code'],
                'gtin' => $product['product_code'],
                'used' => 0,
                'adult' => 'no'
            ];
        }

        return $productsResult;
    }

    /**
     * Retrieves the full category path based on SEO path.
     *
     * @param string $seo_path SEO path of the category.
     * @param string $lang_code Language code for localization.
     * @return array An array containing the full category path.
     */
    private static function category_full($seo_path, $lang_code): array
    {
        $explode_seo_patch = explode("/", $seo_path);
        $category_full_array = array();
        foreach ($explode_seo_patch as $item) {
            $fn_get_categories_list = fn_get_categories_list($item, $lang_code);
            $test = $fn_get_categories_list["$item"];
            $category_full_array[] = $test;
        }

        return $category_full_array;
    }

    /**
     * Retrieves the image path of a product.
     *
     * @param array $product Product data.
     * @return string|null Image path if available, otherwise null.
     */
    private static function get_image(array $product): ?string
    {
        $image_path = null;
        if (array_key_exists("main_pair", $product)) {
            $main_pair = $product["main_pair"];
            $detailed = $main_pair["detailed"];
            $image_path = $detailed["image_path"];
        }

        return $image_path;
    }

    /**
     * Retrieves the last category in the path based on SEO path.
     *
     * @param string $seo_path SEO path of the category.
     * @param string $lang_code Language code for localization.
     * @return string Last category in the path.
     */
    private static function category($seo_path, $lang_code)
    {
        $explode_seo_patch = explode("/", $seo_path);

        $myLastElement = end($explode_seo_patch);
        $fn_get_categories_list = fn_get_categories_list($myLastElement, $lang_code);
        return $fn_get_categories_list[$myLastElement];
    }

    /**
     * Retrieves the value of a product feature by feature ID.
     *
     * @param array $product The array of product data.
     * @param int $feature_id The feature ID.
     * @return string|null The value of the product feature, or null if the feature is not found.
     */
    private static function get_features(array $product, int $feature_id): ?string
    {
        // We can assume that the product has the product_features key
        $product_features = $product['product_features'];
        // Check if the product_features key has the feature ID as key
        return $product_features[$feature_id]['variant'] ?? null;
    }
}
