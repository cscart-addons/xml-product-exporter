# XML Product Exporter

## Description

XML Product Exporter for CS-Cart is an addon that enables you to generate XML files for price comparison platforms. This addon facilitates the export of goods for various platforms such as Salidzini.lv and Kurpirkt.lv.

## Installation

Download the addon archive from GitHub.

### Manual Installation Option 1

1. Unpack the archive and copy the contents to the addons directory of your CS-Cart store.
2. Log in to the admin panel of your store.
3. Go to **Modules** > **Manage Modules**.
4. Find "XML Product Exporter" in the list of modules and click on **Install**.

### Automatic Installation Option 2

1. Log in to the admin panel of your store.
2. Navigate to **Modules** > **Manage Modules**.
3. Click on **+** and select the downloaded archive.
4. Click on **Install**. The system will install XML Product Exporter.

## Settings

After installation, follow these steps:
1. Go to **Settings** > **Addon Settings** > **XML Product Exporter**.
2. Enable export for the required platforms (Salidzini.lv, Kurpirkt.lv).
3. Specify export parameters, such as minimum and maximum prices, quantity of goods, and others.
4. Save your changes.

## Usage

Once the addon is configured, it will automatically generate XML files for the selected platforms.
Contact the manager of your chosen platform to provide them with the generated XML files.
The link for the platform can be obtained under the settings of the selected platform.

## Compatibility

XML Product Exporter is compatible with CS-Cart versions from 4.0 to 4.15.2. Testing has not been conducted on higher versions.

## Support

For any inquiries, please contact us via email at [dev.oleg.kosarev@outlook.com](mailto:dev.oleg.kosarev@outlook.com).

## Roadmap

- [ ] Make select choices specific categories for each platform (like Salidzini.lv and Kurpirkt.lv) and add them to the settings.
- [ ] Add a link to the documentation for each platform.
- [ ] Add a link to the changelog for each platform.
- [ ] Add a link to the source code for each platform.
- [ ] Add a link to the issue tracker for each platform.

## Contributing

We welcome contributions to enhance the project! Whether it's fixing bugs, adding new features, or improving documentation, your input is valuable to us. Please refer to the contribution guidelines in the repository and let's collaborate to make this project even better together. Thank you for your support!

## Authors and Acknowledgment

**Author:** Oleg Kosarev

**Email:** [dev.oleg.kosarev@outlook.com](mailto:dev.oleg.kosarev@outlook.com)

## License

Gnu General Public License v3 or later.
If you wish to use XML Product Exporter for CS-Cart, you can do so under the terms of the GNU General Public License v3 or later. Closed-source usage is prohibited. Refer to the [LICENSE](https://gitlab.com/cscart-addons/xml-product-exporter/-/raw/main/LICENSE?ref_type=heads) for more details.

## Project Status

The project is in an active development phase. We are continually working on enhancing functionality, addressing identified issues, and striving to achieve our objectives. Our team is fully committed to success and delivering a quality project.
Next steps include completing current tasks, conducting testing, and preparing for the next development phase.
